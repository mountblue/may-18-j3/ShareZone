import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';

import Routes from './components/routes';
import Mystore from './store';


class App extends Component {
  render() {
    return (
      <Provider store={Mystore}>
        <Routes />
      </Provider>

    );
  }
}

export default App;
