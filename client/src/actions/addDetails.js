import { ADD_DETAILS } from './types';

export const addDetails = (name,id,index) => dispatch => {
      dispatch({
        type: ADD_DETAILS,
        payload: {
            index: index,
            name: name,
            userId: id
        }
      })
};