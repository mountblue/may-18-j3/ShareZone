export const NEW_MEMBER = 'NEW_MEMBER';
export const DELETE_MEMBER = 'DELETE_MEMBER';
export const ADD_DETAILS = 'ADD_DETAILS';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_GROUPS = 'FETCH_GROUPS';
