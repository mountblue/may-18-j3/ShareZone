import { DELETE_MEMBER } from './types';

export const deleteMember = (index) => dispatch => {
      dispatch({
        type: DELETE_MEMBER,
        payload: index
      })
};