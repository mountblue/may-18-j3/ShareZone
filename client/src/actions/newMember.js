import { NEW_MEMBER } from './types';

export const newMember = () => dispatch => {

  return (dispatch({
    type: NEW_MEMBER,
    payload: {
      name: '',
      userId: ''
    }
  }))
};