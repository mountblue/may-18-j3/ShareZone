import { FETCH_USER } from './types';

export const fetchUser = () => dispatch => {
    //   fetch('https://jsonplaceholder.typicode.com/posts')
    //     .then(res => res.json())
    //     .then(posts => {
    //       dispatch({
    //         type: FETCH_POSTS,
    //         payload: posts
    //       })
    //     });
    dispatch({
        type: FETCH_USER,
        payload: {
            name: 'Jijendran',
            userId: 'Jijendran.kk@mountblue.io'
        }
    })
};