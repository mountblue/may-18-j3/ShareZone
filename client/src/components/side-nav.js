import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUser } from '../actions/fetchUser';
import { Link } from 'react-router-dom';
import '../styles/header.css';

class Sidenav extends Component {

    constructor(props) {
        super(props);
        this.state = {
            groupsname: []
        }
    }

    componentDidMount() {
        
        this.props.fetchUser();
        console.log(this.props.userId, "sidenav")

        fetch('http://10.1.0.211:8085/group/'+ this.props.userId)
        .then(res => res.json())
        .then(data => this.setState({ groupsname:data}));

        // this.setState({ groupsname: ["Mountblue", "Coffee Lovers", "Tea"] });
        // this.setState({ groupsname: Object.values(this.props.groups) });
        
    }

    render() {
        const groupsList = this.state.groupsname.map((mem, index) => (
            <div className="sidenav-groups" key={index}>
                <i className="fas fa-tag"></i>
                <p>{mem}</p>
            </div>
        ));

        return (
            <div className="sidenav">
                <div><a className="dashboard-label" href="#dashboard">Dashboard</a></div>
                <div className="groups-label">
                    <i className="fas fa-users"></i>
                    <a href="#groups">Groups</a>
                    <Link className="add-group-butt" to="/creategroup">
                        <button>+ add</button>
                    </Link>
                </div>
                {groupsList}
                <div><a href="#about">About Us</a></div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userId: state.user.user.userId
});

// export default Sidenav;
export default connect(mapStateToProps, { fetchUser })(Sidenav);