import React, { Component } from 'react';
import logo from '../img/logo.png';
import '../styles/log-in.css';
import { Link } from 'react-router-dom';

class Login extends Component {

    render() {
        console.log(this.props);
        return (
            <div class="Log-in">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <span className="title">&#36;hareZone</span>
                </header>
                <div className="log-intro">
                    <p className="log-title">Log into your Account.!</p>
                    <p id="User-name" className="Fields-name">Username/Email</p>
                    <input id="User-field" type="text" placeholder="Enter your Name or Email ID" className="Field-value" autoFocus></input>
                    <p id="Password" className="Fields-name">Password</p>
                    <input id="Pass-field" type="password" placeholder="Minimum 6 character" className="Field-value"></input>
                    <Link className="login" to = "/home">
                        <button type="submit">Log In</button>
                    </Link>
                </div>
            </div>
        );
    }

}

export default Login;