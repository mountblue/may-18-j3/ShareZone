import React, { Component } from 'react';
import logo from '../img/logo.png';
import '../styles/header.css';

class Header extends Component {

    render() {

        return (
            <div className="dashboard">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <span className="Title">&#36;hareZone</span>
                </header>
            </div>
        );
    }

}

export default Header;