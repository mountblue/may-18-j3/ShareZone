import React, { Component } from 'react';
import { connect } from 'react-redux';
import { newMember } from '../actions/newMember';
import { fetchUser } from '../actions/fetchUser';
import logo from '../img/logo.png';
import { Link } from 'react-router-dom';
import '../styles/groups.css';
import Header from './header';
import GroupMember from './groupmember';

class Creategroups extends Component {

    constructor(props) {
        super(props);
        this.state = {
            groupname: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    componentDidMount() {
        this.props.fetchUser();
    }

    onSubmit(e) {
        e.preventDefault();

        const groupMembers = this.props.members.map((mem) => {
            return mem;
        })
        groupMembers.unshift(this.props.user);

        const sendData = {
            groupname: this.state.groupname,
            members: groupMembers
        }
        console.log(sendData, "Submit method call");
        fetch('http://10.1.0.211:8085/group', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'content-type': 'application/json'
            },
            body: JSON.stringify(sendData)
        })
            .then(res => res.json())
            .then(data => console.log(data))

        this.props.history.push(`/groups`);

    }
    handleEvent = () => {
        this.props.newMember();
    }

    render() {

        return (
            <div className="groups">
                <Header />
                <img src={logo} className="group-logo" alt="logo" />
                <div className="create-group">
                    <p className="new-group">Start a new group..!</p>
                    <form onSubmit={this.onSubmit}>
                        <div className="group-name">
                            <label>My group named as </label>
                            <input
                                type="text"
                                name="groupname"
                                autoFocus
                                autoComplete="off"
                                placeholder="Group name"
                                onChange={this.onChange}
                                value={this.state.groupname}
                            />
                        </div>
                        <div className="group-member">
                            <p className="groupmem">Group members</p>
                            <input type="text" defaultValue={this.props.user.name} />
                            <input type="text" defaultValue={this.props.user.userId} />
                        </div>
                        {this.props.members.map((member, index) => <GroupMember key={index} index={index} />)}
                        <button className="new-member" type="button" onClick={this.handleEvent}>+ Add person</button><br />
                        {/* <Link to="/groups"> */}
                        <button className="group-save" type="submit">Save</button>
                        {/* </Link> */}

                    </form>
                </div>
            </div>

        );
    }

}

const mapStateToProps = state => ({
    members: state.member.members,
    user: state.member.user
});

const mapDispatchToProps = {
    newMember,
    fetchUser
};

export default connect(mapStateToProps, mapDispatchToProps)(Creategroups);