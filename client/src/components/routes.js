import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Welcome from './welcome-page';
import Signup from './sign-up';
import Login from './log-in';
import Dashboard from './dashboard';
import Creategroups from './creategroups';
import Groups from './groups-page';

class Routes extends Component {

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path='/' exact strict component={Welcome} />
                    {/* <Route path='/signup' render ={(props)=>{
                        props.name = this.state.name;
                        return <Signup {...props}/>
                        }} /> */}
                    <Route path='/signup' exact strict component={Signup} />
                    <Route path='/login' exact strict component={Login} />
                    <Route path='/home' exact strict component={Dashboard} />
                    <Route path='/creategroup' exact strict component={Creategroups} />
                    <Route path='/groups' exact strict component={Groups} />
                    
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Routes;