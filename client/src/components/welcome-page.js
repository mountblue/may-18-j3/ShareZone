import React, { Component } from 'react';
import '../styles/welcome-page.css';
import logo from '../img/logo.png';
import { Link } from 'react-router-dom';

class Welcome extends Component {

    render() {

        return (
            <div className="Welcome-page">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <span className="title">&#36;hareZone</span>
                    <a className="About" href="#">About Us</a>
                    <Link className="Log" to="/login">
                        <a>Log In</a>
                    </Link>
                </header>
                <div className="App-intro">
                    <p className="Welcome">Welcome to &#36;hareZone</p>
                    <p className="Sub-title1">"Giving is not just about make a donation,</p>
                    <p className="Sub-title2">it's about making a DIFFERENCE"</p>
                    <Link className="Join" to="/signup">
                        <button>Join Us Now</button>
                    </Link>
                </div>
            </div>
        );
    }

}

export default Welcome;