import React, { Component } from 'react';
import logo from '../img/logo.png';
import '../styles/sign-up.css';
import { Link } from 'react-router-dom';

class Signup extends Component {

    render() {

        return (
            <div className="Sign-up">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <span className="Title">&#36;hareZone</span>
                </header>
                <div className="Sign-intro">
                    <p className="Sign-title">Create Your New Account</p>
                    <p className="Sign-discription">Find your friends/family, make a group, share your expenses. Do it all with &#36;hareZone. Already have an account?
                    <Link className="login-link" to="/login">
                            <a>Log In</a>
                        </Link>
                    </p>
                    <p id="User-name" className="Fields-name">Username</p>
                    <input id="User-field" type="text" placeholder="Enter your Name" className="Field-value" autoFocus></input>
                    <p id="Email-id" className="Fields-name">Email</p>
                    <input id="Email-field" type="text" placeholder="Enter your Email Address" className="Field-value"></input>
                    <p id="Password" className="Fields-name">Password</p>
                    <input id="Pass-field" type="password" placeholder="Minimum 6 character" className="Field-value"></input>
                    <Link className="login" to = "/home">
                        <button type="submit">Get Started!</button>
                    </Link>
                </div>
            </div>
        );
    }

}

export default Signup;