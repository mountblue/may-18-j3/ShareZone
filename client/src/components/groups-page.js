import React, { Component } from 'react';
import Header from './header';
import groupImage from '../img/group.jpg';
import proImage from '../img/pro.gif';
import '../styles/groups-page.css';
import { Link } from 'react-router-dom';
import Sidenav from './side-nav';

class Groups extends Component {

    constructor(props) {
        super(props);
        this.state = {
            groupname: 'Mountblue',
            members: []
        }
    }

    componentDidMount() {
        // fetch('https://jsonplaceholder.typicode.com/posts')
        // .then(res => res.json())
        // .then(data => this.setState({posts: data}));

        this.setState({ members: ["Jijendran", "Prabha", "Megha"] });
    }

    render() {
        const groupMembers = this.state.members.map((mem, index) => (
            <div className="group-members" key={index}>
                {/* <img src={proImage} className="profile-img" alt="not found" /> */}
                <p className="member">{mem}</p>
                <p>settled up</p><hr />
            </div>
        ));

        return (
            <div className="group-details">
                <Header />
                <Sidenav />
                <img src={groupImage} className="group-img" alt="not found" />
                <div className="group-msg">
                    <p className="bill-status">You have not added any expenses yet</p>
                    <p className="bill-msg">To add a new expenses. click the "add a bill" button </p>
                </div>
                <div className="currgroup-name">
                    <div className="currgroup-div">
                        <p>{this.state.groupname}</p>
                        <Link className="bill" to="/groups">
                        <button type="button">Add a Bill</button>
                    </Link>
                    <Link className="settle-up" to="/groups">
                        <button type="button">Settle up</button>
                    </Link>
                    </div>
                    <hr />
                </div>
                <div className="group-balances">
                    <h3>Group Balances</h3><hr />
                    {groupMembers}
                </div>
            </div>
        );
    }

}

export default Groups;