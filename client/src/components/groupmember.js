import React, { Component } from 'react';
import { connect } from 'react-redux';
import {deleteMember} from '../actions/deleteMember';
import {addDetails} from '../actions/addDetails';

class GroupMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            membername: '',
            memberId: ''
        };

        this.onChange = this.onChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
        this.props.addDetails(this.state.membername, this.state.memberId, this.props.index);
    }

    handleDelete(){
        this.props.deleteMember(this.props.index);
    }
    render() {
        return (
            <div className="group-member">
                <input
                    type="text"
                    name="membername"
                    autoComplete = "off"
                    placeholder="user name"
                    onChange={this.onChange}
                    value={this.state.membername}
                />
                <input
                    type="text"
                    name="memberId"
                    autoComplete = "off"
                    placeholder="email id"
                    onChange={this.onChange}
                    value={this.state.memberId}
                />
                <button type="reset" onClick={this.handleDelete}>&#9747;</button>
            </div>
        )
    }
}

const mapDispatchToProps = {
    deleteMember,
    addDetails
};

export default connect(null, mapDispatchToProps)(GroupMember);

