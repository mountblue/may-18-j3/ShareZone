import { combineReducers } from 'redux';
import members from './memberReducer';
import currUser from './userReducer';

export default combineReducers({
  member: members,
  user: currUser
});
