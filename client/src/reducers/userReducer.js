import { FETCH_USER } from '../actions/types';

const initialState = {
    user: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_USER:
            let currUser = action.payload;
            console.log(action.payload, "fetchUser")
            return {
                ...state,
                user: currUser
            };
        default:
            return state;
    }
}