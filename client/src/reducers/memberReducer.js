import { NEW_MEMBER, DELETE_MEMBER, ADD_DETAILS, FETCH_USER } from '../actions/types';

const initialState = {
    members: [],
    user: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case NEW_MEMBER:
            let member = state.members.slice(0)
            member.push(action.payload)
            return {
                ...state,
                members: member
            };
        case ADD_DETAILS:
            let updateMember = state.members.slice(0);
            updateMember[action.payload.index].name = action.payload.name;
            updateMember[action.payload.index].userId = action.payload.userId;
            return {
                ...state,
                members: updateMember
            };
        case DELETE_MEMBER:
            let deleteMember = state.members.slice(0)
            deleteMember.splice((action.payload), 1)
            return {
                ...state,
                members: deleteMember
            };
        case FETCH_USER:
            let groupMember = state.members.slice(0);
            let currUser = action.payload;
            groupMember.push(currUser);
            return {
                ...state,
                user: currUser
            };
        default:
            return state;
    }
}
