# ShareZone

sharezone is an app for splitting expenses with your friends. It lets you and your friends add various bills and keep track of who owes who, and then it helps you to settle up with each other.

### sharezone architecture:
#### front-end:
    - CSS
    - React
    - React-redux
    - React-router
    - Redux-thunk
#### back-end:
    - Node
    - MongoDB
    
### user persona
- user:user can create group,add bill and settle up the bill 
### installation
1.install the following parts
    -node
    -npm 
2.clone with git
```sh
    $ git clone repo-path
```
3.change the directory 
```sh
    $ cd ShareZone 
```
4.install all require libraries
```sh
    $ npm install   
```
5.run the server in client side
```sh
    $ npm start
```
6.run the server in back-end side
```sh
    $ node server.js
```

### feature
 - user can create account in ShareZone
 - user can add members to group
 - user can add bill,ShareZone SBAT update everyone's balance to keep track of how much each person owes
 - user can settle up the balance payment

### todo
- edit the group,bill
- can pay money through e-transfer
- Test all the functionalities with testing tools
### known bugs
- add responsiveness of the website
- re-rendering didn't work properly in settle up part
### License
License Copyright 2018 Jijendran.K.K,Lahari,Dibyendu
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



