const express = require('express');
const app = express()
const cookiePareser= require('cookie-parser')
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const User = require('./models/user-model');
mongoose.connect('mongodb://dibyendu.kundu:alpha6delta91@ds163730.mlab.com:63730/sharezone', () => {
    console.log('connected to mongodb');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookiePareser())


app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
    done(null, user.username);
});

passport.deserializeUser((id, done) => {
    done(null, id)
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, X-Custom-Header, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, XMODIFY");
    res.header("cookies_needed", true);
    next();
});

app.use(session({
    secret: 'keyboard cat'
}));

passport.use(new LocalStrategy(

    function (username, password, cb) {

        getUser(username, password).then((user) => {
            cb(null, user)
        }).catch((err) => {
            console.log(err)
            cb(err)
        })
    }
));
const getUser = (username, password) => {
    return new Promise(function (resolve, reject) {
        User.find({
            username: username
        }, function (err, user) {
            if (user == '') {
                reject({
                    error: false
                })

            } else if (user[0].username == username) {

                if (user[0].password == password) {
                    console.log('successful')
                    resolve({
                        username: user[0].username,
                        usermail: user[0].usermail,
                        logstatus:true
                    })
                } else {
                    console.log('not successful')
                    reject({
                        error:"invalid username or password"
                        
                    })
                }

            } else {
                console.log('not successful')
                reject({
                    error:"invalid username or password"
                })
            }
        })


    })
}



app.post('/login', (req, res, next) => {
    if((JSON.stringify(req.body)=='{}') || (req.body.username=="") || (req.body.usermail=="") || (req.body.password=="")){
    let logdata={logstatus:false}

   
    res.cookie('connect.sid', '');
    console.log(res.cookies)
    return res.status(404).send({
        logdata
    });
}
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            let logdata={logstatus:false}
            console.log('hello')
            res.cookie("connect.sid","");
            return res.status(404).send({
                logdata
            });
        } else {
            req.logIn(user, function (err) {
                if (err) {
                    res.cookie("connect.sid","");
                   
                    return next(err);}
                else{
                let logdata={username:user.username,usermail:user.usermail,logstatus:user.logstatus}
                
                return res.status(200).send({
                    logdata
                });
            }
            })
        }
    })(req, res, next);
    
       
    
})  
app.post('/logout', (req, res) => {
console.log(req.cookies['connect.sid'])
//res.cookie("connect.sid","");
res.clearCookie('connect.sid')
res.send('cookie cleared')
.send(req.cookies['cookie cleared'])
})


app.post('/signup', (req, res) => {
    //console.log(req.body)
    let tempdata = {
        username: req.body.username,
        usermail: req.body.usermail
    }
    //let encryptedpass = CryptoJS.AES.encrypt(pass, 'secret key 123');
    if (req.body.username != '') {
        User.findOne({
            username: req.body.username
        }, (err, data) => {
            console.log(data + 'this is data')
            if (data == null) {
                new User({
                    username: req.body.username,
                    usermail: req.body.usermail,
                    password: req.body.password
                }).save().then((data) => {
                    res.status(200).send(tempdata)
                })
            } else {
                res.status(409).send({
                    error: "username already exists"
                });
            }
        })
    } else {
        res.status(409).send({
            error: "nousername"
        });
    }

});


app.listen(4000, () => {
    console.log('app now listening for requests on port 4000');
});