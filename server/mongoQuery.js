let mongoose = require('mongoose');
let path = require('path');
let groups = require('./models/groupNames');
let bills=require('./models/bills');
let splitBill = require('./models/splitBill');
let settleUp = require('./models/settleUp');
const ObjectId = mongoose.Types.ObjectId;

//make a connection..
mongoose.connect('mongodb://dibyendu.kundu:alpha6delta91@ds163730.mlab.com:63730/sharezone', () => {
   console.log('connected to mongodb');
});
let dbStatus = mongoose.connection;
dbStatus.on('error', console.error.bind(console, "connection error"));
dbStatus.once('open', function () {
  console.log("Connection to testDb is open...");
});
addingGroup = (result) => 
{
	return new Promise(function(resolve,reject){
		let insertgroup = new groups(result);
		insertgroup.save().then(function(data){
			resolve('group added');
		})
	}).then(function(data){
		if(data == 'group added')
		{
			return new Promise(function(resolve,reject){
				groups.aggregate([{$match:{'groupname':result.groupname}},{$project:{_id:1,groupname:1}}],function(err,data){
					if(err)
						reject(err);
					else
						resolve(JSON.stringify(data[0]));
				})
			})
		}
	}).then(function(data){
		return new Promise(function(resolve,reject){
			let groupdata = JSON.parse(data);
			let obj={};
			obj[groupdata.groupname] = groupdata._id;
			resolve(obj);
		})
	})
}

fetchingGroups = (result) =>
{
	console.log(result);
	return new Promise(function(resolve,reject){
		groups.aggregate([{'$match':{'members.userId':result}},{'$project':{groupname:1,_id:0}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data));
		})
	}).then(function(data){
		return new Promise(function(resolve,reject){
			let groupnames = JSON.parse(data);
			let groupArray=[];
			for(let obj of groupnames)
			{
				groupArray.push(obj.groupname);
			}
			resolve(groupArray);
		})
	})
}

addingBill = (result) => 
{
	return new Promise(function(resolve,reject){
		let insertBill = new bills(result);
		insertBill.save().then(function(data){
			resolve(result);
		})
	}).then(function(bill){
		splitAmount(bill);
	})
}

function splitAmount(bill)
{
	return new Promise(function(resolve,reject){
		groups.aggregate([{'$match':{'_id':ObjectId(bill.id)}},{'$project':{members:1,_id:0}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data[0].members));
		})
	}).then(function(members){
		return new Promise(function(resolve,reject){
			splitBill.aggregate([{$match:{'id':bill.id}}],function(err,data){
				if(err)
					reject(err);
				else
				{
					if(JSON.stringify(data) == '[]')
						insertSplitBill(members,bill.amount,bill.paidBy,bill.id);
					else
						updateSplitBill(members,bill.amount,bill.paidBy,bill.id);
				}
			})
		})
	})
}

function insertSplitBill(members,amount,paidBy,id)
{
	console.log(members);
	let data=JSON.parse(members);
	let membersArray=[];
	for(let obj of data)
	{
		membersArray.push(obj.name);
	}
	let memberCombo=[];
	for(let i=0;i<membersArray.length;i++)
	{	
		for(let j=i+1;j<membersArray.length;j++)
		{
			memberCombo.push([membersArray[i],membersArray[j],0]);
		}
	}
	return new Promise(function(resolve,reject){
		let splitBills= new splitBill({'id':id,'members':memberCombo});
		splitBills.save().then(function(data){
			resolve('inserted');
		})
	}).then(function(data){
		if(data == 'inserted')
		{
			updateSplitBill(members,amount,paidBy,id);
		}
	})
}

function updateSplitBill(memberLength,amount,paidBy,id)
{
	let memberLen=JSON.parse(memberLength);
	return new Promise(function(resolve,reject){
		splitBill.aggregate([{$match:{'id':id}},{$project:{members:1}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data[0].members));
		})
	}).then(function(members){
		return new Promise(function(resolve,reject){
			splitBill.remove({'id':id},function(err,data){
				if(err)
					reject(err);
				else
					resolve('document deleted');
			});
		}).then(function(data){
			if(data == 'document deleted')
			{
				let memberArray=JSON.parse(members);
				console.log(memberArray.length);
				for(let i=0;i<memberArray.length;i++)
				{
					if(memberArray[i].includes(String(paidBy))== true)
					{
						if(memberArray[i].indexOf(String(paidBy)) == 0)
						{
							memberArray[i][2] = memberArray[i][2]+(Number(amount)/memberLen.length);
						}
						else
						{
							memberArray[i][2] = memberArray[i][2]-(Number(amount)/memberLen.length);
						}
					}
				}
				console.log(memberArray);
				let splitBills= new splitBill({'id':id,'members':memberArray});
				splitBills.save();
			}
		})
	})
}

fetchingBill = (result) => 
{
	console.log(result);
	return new Promise(function(resolve,reject){
		groups.aggregate([{'$match':{'_id':ObjectId(result)}},{'$project':{'count':{$size:'$members'}}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data[0].count));
		})
	}).then(function(memberSize){
		return new Promise(function(resolve,reject){
			bills.aggregate([{'$match':{'id':result}},{'$project':{_id:0,__v:0}}],function(err,data){
				if(err)
					reject(err);
				else
					resolve({"responseMessage":"data fetched","count":memberSize,"data":data});
			})
		})
	})	
}

fetchingBalance = (result) =>
{
	return new Promise(function(resolve,reject){
		groups.aggregate([{'$match':{'_id':ObjectId(result)}},{'$project':{members:1,_id:0}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data[0].members));
		})
	}).then(function(members){
		console.log(members);
		let data=JSON.parse(members);
		let membersArray=[];
		for(let obj of data)
		{
			membersArray.push(obj.name);
		}
		return new Promise(function(resolve,reject){
		splitBill.aggregate([{$match:{'id':result}},{$project:{_id:0,__v:0}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data[0].members));
		})
	}).then(function(memberCombo){
		return new Promise(function(resolve,reject){
			membersCombo=JSON.parse(memberCombo);
			let balance=[];
			let splitBalance=[];
			for(let i=0;i<membersArray.length;i++)
			{
				let netBalance=0;
				let bal=[];
				for(let j=0;j<membersCombo.length;j++)
				{
					if(membersCombo[j].includes(membersArray[i]) == true)
					{
						if(membersCombo[j].indexOf(membersArray[i]) == 0)
						{
							netBalance+=membersCombo[j][2];
							bal.push([membersCombo[j][1],membersCombo[j][2]]);
						}
						else
						{
							netBalance-=membersCombo[j][2];
							bal.push([membersCombo[j][0],-membersCombo[j][2]]);
						}
					}
				}
				balance.push([membersArray[i],netBalance]);
				splitBalance.push({'paidMember':membersArray[i],'bal':bal});
			}
			resolve({'responseMessage':'balance fetched','balance':balance,'splitBalance':splitBalance});
		})
	})
	})
}

settleup = (result) =>{
	return new Promise(function(resolve,reject){
		let insertData = new settleUp(result);
		insertData.save().then(function(data){
			resolve(result);
		})
	}).then(function(settleupData){
		settleBill(settleupData);
	})
}

function settleBill(settleupData)
{
	return new Promise(function(resolve,reject){
		splitBill.aggregate([{$match:{id:settleupData.id}},{$project:{members:1}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve(JSON.stringify(data[0].members));
		})
	}).then(function(members){
		let memberArray = JSON.parse(members);
		return new Promise(function(resolve,reject){
			splitBill.remove({'id':settleupData.id},function(err,data){
				if(err)
					reject(err);
				else
					resolve('document deleted');
			});
		}).then(function(data){
			if(data == 'document deleted')
			{
				for(let i=0;i<memberArray.length;i++)
				{
					if(memberArray[i].includes(String(settleupData.received)) == true && memberArray[i].includes(String(settleupData.paid)) == true)
					{
						if(memberArray[i].indexOf(String(settleupData.paid)) == 0)
						{
							memberArray[i][2] = memberArray[i][2] + Number(settleupData.amount);
						}
						else
						{
							memberArray[i][2] = memberArray[i][2] - Number(settleupData.amount);
						}
					}
				}
			}
			let splitBills= new splitBill({'id': settleupData.id,'members':memberArray});
			splitBills.save();
		})
	})
}

fetchingSettledBill = (result) =>
{
	return new Promise(function(resolve,reject){
		settleUp.aggregate([{$match:{'id':result}},{$project:{_id:0,__v:0}}],function(err,data){
			if(err)
				reject(err);
			else
				resolve({'settleUpData':data});
		})
	})
}

module.exports = {addingGroup,fetchingGroups,addingBill,fetchingBill,fetchingBalance,settleup,fetchingSettledBill}