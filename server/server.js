const express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let app = express();
const cookiePareser= require('cookie-parser')
const passport = require('passport');
const session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;
let Cors = require('cors');
let calculation = require('./mongoQuery.js');
const port = 4000;
const User = require('./models/user-model');
mongoose.connect('mongodb://dibyendu.kundu:alpha6delta91@ds163730.mlab.com:63730/sharezone', () => {
    console.log('connected to mongodb');
});
app.use(Cors());
app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookiePareser())
app.use(passport.initialize());
app.use(passport.session());


passport.serializeUser((user, done) => {
    done(null, user.username);
});

passport.deserializeUser((id, done) => {
    done(null, id)
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, X-Custom-Header, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, XMODIFY");
    res.header("cookies_needed", true);
    next();
});

app.use(session({
    secret: 'keyboard cat'
}));


//passport

passport.use(new LocalStrategy(

    function (username, password, cb) {

        getUser(username, password).then((user) => {
            cb(null, user)
        }).catch((err) => {
            console.log(err)
            cb(err)
        })
    }
));
const getUser = (username, password) => {
    return new Promise(function (resolve, reject) {
        User.find({
            username: username
        }, function (err, user) {
            if (user == '') {
                reject({
                    error: false
                })

            } else if (user[0].username == username) {

                if (user[0].password == password) {
                    console.log('successful')
                    resolve({
                        username: user[0].username,
                        usermail: user[0].usermail,
                        logstatus:true
                    })
                } else {
                    console.log('not successful')
                    reject({
                        error:"invalid username or password"
                        
                    })
                }

            } else {
                console.log('not successful')
                reject({
                    error:"invalid username or password"
                })
            }
        })


    })
}



//creating the group and adding the members
app.post('/group',function(req,res)
{
	calculation.addingGroup(req.body).then(function(data){
		res.send(data);
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})

app.get('/group/:userId',function(req,res){
	calculation.fetchingGroups(req.params.userId).then(function(data){
		res.send(data);
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})

app.post('/bills',function(req,res){
	calculation.addingBill(req.body).then(function(data){
		res.send({'responseMessage':'bill added'});
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})

app.get('/bills/:id',function(req,res){
	calculation.fetchingBill(req.params.id).then(function(data){
		res.send(data);
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})

app.get('/bills/balance/:id',function(req,res){
	calculation.fetchingBalance(req.params.id).then(function(data){
		res.send(data);
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})

app.post('/settleUp',function(req,res){
	calculation.settleup(req.body).then(function(data){
		res.send({'responseMessage':'bill was settled'});
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})

app.get('/settleUp/:id',function(req,res){
	calculation.fetchingSettledBill(req.params.id).then(function(data){
		res.send(data);
	}).catch(function(err){
		res.status(400).send("invalid data");
	})
})




//login


app.post('/login', (req, res, next) => {
    if((JSON.stringify(req.body)=='{}') || (req.body.username=="") || (req.body.usermail=="") || (req.body.password=="")){
    let logdata={logstatus:false}

   
    res.cookie('connect.sid', '');
    console.log(res.cookies)
    return res.status(404).send({
        logdata
    });
}
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            let logdata={logstatus:false}
            console.log('hello')
            res.cookie("connect.sid","");
            return res.status(404).send({
                logdata
            });
        } else {
            req.logIn(user, function (err) {
                if (err) {
                    res.cookie("connect.sid","");
                   
                    return next(err);}
                else{
                let logdata={username:user.username,usermail:user.usermail,logstatus:user.logstatus}
                
                return res.status(200).send({
                    logdata
                });
            }
            })
        }
    })(req, res, next);
    
       
    
}) 

//logout

app.post('/logout', (req, res) => {
console.log(req.cookies['connect.sid'])
//res.cookie("connect.sid","");
res.clearCookie('connect.sid')
res.send('cookie cleared')
.send(req.cookies['cookie cleared'])
})

//signup

app.post('/signup', (req, res) => {
    //console.log(req.body)
    let tempdata = {
        username: req.body.username,
        usermail: req.body.usermail
    }
    //let encryptedpass = CryptoJS.AES.encrypt(pass, 'secret key 123');
    if (req.body.username != '') {
        User.findOne({
            username: req.body.username
        }, (err, data) => {
            console.log(data + 'this is data')
            if (data == null) {
                new User({
                    username: req.body.username,
                    usermail: req.body.usermail,
                    password: req.body.password
                }).save().then((data) => {
                    res.status(200).send(tempdata)
                })
            } else {
                res.status(409).send({
                    error: "username already exists"
                });
            }
        })
    } else {
        res.status(409).send({
            error: "nousername"
        });
    }

});


app.listen(port, function () {
	console.log("running server on " + port);
})