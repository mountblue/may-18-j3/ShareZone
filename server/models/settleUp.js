let mongoose=require('mongoose');
let Schema = mongoose.Schema;
//define the schema 
let settleUpSchema = new Schema({
   id:String,
   amount:Number,
   paid:String,
   received:String,
   date:String
});

module.exports = mongoose.model('settleUp', settleUpSchema,'settleUpData');