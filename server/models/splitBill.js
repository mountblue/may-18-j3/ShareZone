let mongoose=require('mongoose');
let Schema = mongoose.Schema;
//define the schema for bill collection
let splitBillSchema = new Schema({
   id:String,
   members:Array
});

module.exports = mongoose.model('splitBill', splitBillSchema,'splitBillData');