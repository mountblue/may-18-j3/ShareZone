let mongoose=require('mongoose');
let Schema = mongoose.Schema;
//define the schema for groupname collection
let billSchema = new Schema({
   description:String,
   id:String,
   paidBy:String,
   amount:Number,
   date:String
});

module.exports = mongoose.model('bills', billSchema,'billsData');