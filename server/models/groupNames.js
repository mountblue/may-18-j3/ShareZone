let mongoose=require('mongoose');
let Schema = mongoose.Schema;
//define the schema for groupname collection
let groupSchema = new Schema({
   groupname:String,
   members:[{name:String,userId:String}]
});

module.exports = mongoose.model('groups', groupSchema,'groupsData');